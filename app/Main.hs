{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Main where

import Web.Scotty
import Data.IORef
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T

type Database = IORef [(Text, Text)]

main :: IO ()
main = do
  db <- newIORef []

  scotty 3000 $ do
    get "/:key" $ do
      key <- param "key"
      result <- liftIO $ getValue db key
      case result of
        Just value -> text value
        Nothing    -> text "No such key in database, sorry :("

    post "/:key/:value" $ do
      key <- param "key"
      value <- param "value"
      liftIO $ putValue db key value
      text "Successfully added given key-value pair :)"

    notFound $ do
      text "Invalid URL, correct options are localhost:3000/:key for GET and localhost:3000/:key/:value for POST."

getValue :: Database -> Text -> IO (Maybe Text)
getValue db key = do
  database <- readIORef db
  return $ lookup key database

putValue :: Database -> Text -> Text -> IO ()
putValue db key value = do
  database <- readIORef db
  let updated = (key, value) : filter (\(k, _) -> k /= key) database
  writeIORef db updated