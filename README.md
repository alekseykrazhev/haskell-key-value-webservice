# Haskell key-value webservice

Completed implementation of a sample key-value storage webservice for a Functional Programming course

### Startup

To start application run `cabal run webservice` after clonning the repo from `webservice` dir

### Functionality

There are two endpoints available

    - GET requests: /:key
    - POST requests: /:key/:value

To check functionality, use app like *POSTMAN* to send specified requests.

### Screenshots

POST request example

![POST request example](images/image.png)

GET request example

![GET Request example](images/image-1.png)

Invalid endpoit example

![Invalid endpoint example](images/image-2.png)

### Used frameworks and language

    - Haskell
    - Cabal
    - Scotty web framework
    - Persistent
    - Warp
    - newIORef

### Author

* [alekseykrazhev](https://gitlab.com/alekseykrazhev)